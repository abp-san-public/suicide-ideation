import os
import warnings
warnings.filterwarnings("ignore")

import numpy as np
import pandas as pd

from dap.datasets import FeaturesDataset
from dap.metrics import MCC
from dap.utils import get_one_value_columns
from dap.models import get_SVM_classifier
from dap import DAP
# import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

import pickle
# from imblearn.over_sampling import SMOTENC

np.random.seed(1234)

#%% parse cohort
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("gender", help="'females' or 'males'")
parser.add_argument("cohort", help="'national' or 'priority'")
args = parser.parse_args()
gender = args.gender
cohort = args.cohort
assert gender in ['females', 'males']
assert cohort in ['national', 'priority']

dataset_name = f'{gender}_{cohort}'

version = 'v2'

#%% SETTINGS
TARGET = 'suicide_ideation'
N_FEAT_STEP = 30

#%% load data
featuresfile = f'./Data/CSV/datasets/{version}/{dataset_name}/features.csv'
targetsfile = f'./Data/CSV/datasets/{version}/{dataset_name}/targets.csv'

#load features
features = pd.read_csv(featuresfile, index_col=0)

#drop unwanted features
features = features.drop(columns = ['heterosex', 'self_harming'])

#load target
target = pd.read_csv(targetsfile, index_col=0)[TARGET]

# check that no nan are present
assert features.isna().sum().sum() == 0
assert target.isna().sum() == 0

#%% load split indices
idx_train = np.loadtxt(f'./Data/CSV/datasets/{version}/{dataset_name}/idx_train.txt').astype(int)
idx_test  = np.loadtxt(f'./Data/CSV/datasets/{version}/{dataset_name}/idx_test.txt').astype(int)

#%%
#remove 1-value cols
cols_one_value = get_one_value_columns(features.iloc[idx_train,:])
features.drop(columns=cols_one_value, inplace=True)

dataset = FeaturesDataset(features, target, idx_test=idx_test)

#%% SMOTE
# X_train = dataset.features_train
# y_train = dataset.target_train

# is_cat = [x in categorical_features for x in X_train.columns]

# sm = SMOTENC(is_cat)
# X_train_upsampled, y_train_upsampled = sm.fit_resample(X_train, y_train)

# dataset.features_train = X_train_upsampled
# dataset.target_train = y_train_upsampled

#%% normalize features
scaler_features = StandardScaler()
dataset.apply_transformation(scaler_features)

# class_weight = compute_binary_weights(dataset.target_train)

#%% SET ML PARAMS
model, parameters = get_SVM_classifier(class_weight='balanced')
metric = MCC()
parameters['C'] = parameters['C'][:-2]

#%% run training
# (may take a while)
results_dict = DAP(dataset, model, parameters, metric, 
                   n_feat_step=None, importance_mode='permutation',
                   permutation_repeats=10, rank_method='average',
                   proba=True)

results_dict['dataset'] = dataset

#%% save results
outdir = f'./Data/results/{version}/{TARGET}/{dataset_name}'
os.makedirs(outdir, exist_ok=True)

outfile = f'{outdir}/results_dap_noSH'

with open(outfile, 'wb') as f:
    pickle.dump(results_dict, f)
