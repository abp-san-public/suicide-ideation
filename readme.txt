The data are the Colombia National and Priority cohorts of the VACS dataset.
Data are not made available but can be accessed upon request to the VACS (see https://www.togetherforgirls.org/en/analyzing-public-vacs-data).

The machine learning procedure (training, evaluation of the performance, and visualization of the results) is based on a custom library (dap) 
that can be made available upon request to the corresponding author.