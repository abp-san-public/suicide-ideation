#%% import
import os
import pandas as pd

version = 'v2'

target_cols = ['suicide_ideation']
datafile = f'./Data/CSV/processed/{version}.csv'

#%%
for cohort in ['national', 'priority']:
    for gender in ['females', 'males']:

        dataset = f'{gender}_{cohort}'
        print()
        print('+'*20)

        data = pd.read_csv(datafile, index_col=0)
        data = data.query('age >=14')
        
        #% remove cols with many nans and columns with nans
        
        if gender == 'females':
            data = data.query('Sex == 2')
            data.drop(columns = ['Sex'], inplace=True)
        else:
            data = data.query('Sex == 1')
            data.drop(columns = ['Sex', 'CL6_adverse_pregnancy'], inplace=True)
        
        if cohort == 'national':
            data = data.query('code_SUBREGION == -1')
        else:
            data = data.query('code_SUBREGION != -1')
            
        data.drop(columns = ['code_SUBREGION'], inplace=True)
        data.dropna(axis=0, inplace=True)
        
        os.makedirs(f'./Data/CSV/datasets/{version}/{dataset}', exist_ok=True)
        
        targets = data[target_cols]
        features = data.drop(columns = target_cols)
        
        print(features.shape)
        #%
        targets.to_csv(f'./Data/CSV/datasets/{version}/{dataset}/targets.csv')
        features.to_csv(f'./Data/CSV/datasets/{version}/{dataset}/features.csv')



