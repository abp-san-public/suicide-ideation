import numpy as np
np.random.seed(1234)
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

from dap.utils import get_pred_final, get_performance_best, get_p_best

from sklearn.metrics import confusion_matrix

import pickle

from sklearn.metrics import matthews_corrcoef as mcc

def MCC_bootstrap(x):
    return(mcc(x[:,0], x[:,1]))

#%%
version = 'v2'
TARGET = 'suicide_ideation'
    
#%%
outdir = f'./Data/results/{version}/{TARGET}'

for gender in ['females', 'males']:
    for cohort in ['national', 'priority']:
        dataset = f'{gender}_{cohort}'
        outfile = f'{outdir}/{dataset}/results_dap'
        
        print()
        print('+'*20)
        print(dataset)
        with open(outfile, 'rb') as f:
            results_dict = pickle.load(f)
        
        print(get_p_best(results_dict))
        
        p = get_performance_best(results_dict)
        print(p)
        
        pred = get_pred_final(results_dict)
        
        cm_train = confusion_matrix(pred['true_train'], pred['pred_train'])
        acc_train = [100*cm_train[0,0] / (cm_train[0,0] + cm_train[0,1]),
                     100*cm_train[1,1] / (cm_train[1,0] + cm_train[1,1])]
        
        cm_train = pd.DataFrame(cm_train, index = ['Below Median', 'Above Median'], columns = ['Below Median', 'Above Median'])
        cm_train['Class Acc.'] = acc_train
        
        cm_test = confusion_matrix(pred['true_test'], pred['pred_test'])
        acc_test = [100*cm_test[0,0] / (cm_test[0,0] + cm_test[0,1]),
                    100*cm_test[1,1] / (cm_test[1,0] + cm_test[1,1])]
        
        cm_test = pd.DataFrame(cm_test, index = ['Below Median', 'Above Median'], columns = ['Below Median', 'Above Median'])
        cm_test['Class Acc.'] = acc_test
        
        print(cm_train)
        print(cm_test)
        
        n_features = np.min([10, len(results_dict['selection']['features_ranked'])])

#%%