#%%
import numpy as np
np.random.seed(1234)
from dap.plots import plot_rankings_best
from dap.utils import get_rankings_best
import matplotlib.pyplot as plt

import pickle

#%%
version = 'v2'
TARGET = 'suicide_ideation'

#%%
outdir = f'./Data/results/{version}/{TARGET}'

importances = []

for gender in ['females', 'males']:
    for cohort in ['national', 'priority']:
        dataset = f'{gender}_{cohort}'
        outfile = f'{outdir}/{dataset}/results_dap'

        with open(outfile, 'rb') as f:
            results_dict = pickle.load(f)
        
        # f, curr_ax = plt.subplots(1,1)
        plot_rankings_best(results_dict, plot_ci=True, max_features=15)
        importance = get_rankings_best(results_dict)
        importance.to_csv(f'{outdir}/{gender}_{cohort}.csv')
        plt.title(dataset)
        plt.tight_layout()
        