import numpy as np
from sklearn.metrics import matthews_corrcoef as mcc

categorical_features = ['education', 
                        'ever_had_partner', 
                        'safety_community', 
                        'witnessed_repeated_violence_home',
                        'witnessed_repeated_violence_community', 
                        'physical_abuse_peer', 'physical_abuse_peer_child', 
                        'physical_abuse_parent', 'physical_abuse_parent_child', 
                        'physical_abuse_adult', 'physical_abuse_adult_child', 
                        'touching_nopedo', 'touching_pedo',
                        'attempted_nopedo', 'attempted_pedo', 
                        'forcedsex_nopedo', 'forcedsex_pedo', 
                        'pressuredsex_nopedo', 'pressuredsex_pedo',
                        'orphaned', 
                        'employment', 
                        'closeness_mother', 'closeness_father', 'closeness_friends', 
                        'physical_abuse_partner', 
                        'emotional_violence_parents',                        
                        'beating_right', 
                        'physical_punish_acceptable',
                        'using_weapons', 
                        'alcohol_abuse', 
                        'substance_abuse',
                        'had_sex_intercourse_sensitive', 
                        'had_sex_intercourse', 
                        'eterosex', 'omosex', 'bisex', 'asex', 'sex_orientation_not_disclosed',
                        'sex_exploit', 'sex_exploit_sensitive', 
                        'sti',
                        'pregnancy', 'pregnancy_sensitive', 
                        'abortion', 'pregnancy_from_abuse',
                        'physical_violence_against_partner', 
                        'physical_violence_against_other',
                        'sex_violence_against_partner', 
                        'sadness', 'hopelessness', 'self_harming']

dropped_cols = ['bullying', 'material_insecurity', 'emotional_violence_partner', 'currently_having_partner']

C_to_try = [10.0**x for x in np.arange(-5, 3)]
RATIO_TEST = .25
CV_N = 10
CV_K = 5
performance_metric = mcc
select_idx_best_metric = np.argmax

def bootstrap_CI(x, K=0.25, N=100):
    indices = np.arange(len(x))
    K = int(K*len(x))
    out = []
    for i in range(N):
        idx_select = np.random.choice(indices, K)
        x_ = x[idx_select]
        out.append(np.mean(x_))
    return(np.percentile(out, 50), np.percentile(out, 5), np.percentile(out, 95))

def bootstrap_perf(y_true, y_pred, function, K=0.25, N=1000, pred_value = None):
    indices = np.arange(len(y_true))
    K = int(K*len(indices))
    
    out = []
    for i in range(N):
        idx_select = np.random.choice(indices, K)
        y_true_ = y_true[idx_select]
        y_pred_ = y_pred[idx_select]
        out.append(function(y_true_, y_pred_))
    
    out = np.array(out)
    return([np.percentile(out, 50, axis=0), 
            np.percentile(out, 5, axis=0), 
            np.percentile(out, 95, axis=0)])

def compute_metrics(y_true, y_pred):
    MCC = mcc(y_true, y_pred)
    return(np.array([MCC]))

def borda_sort(X, isranking=True):
    #returns the indices that would sort the features
    #according to the borda count 
    X_rank = np.apply_along_axis(np.argsort, 1, X)
    
    n_feat = X_rank.shape[1]
    counts = np.zeros(n_feat)    
    for i_row in range(X_rank.shape[0]):
        for i_col in range(n_feat):
            counts[X_rank[i_row, i_col]] = counts[X_rank[i_row, i_col]] + n_feat - i_col
    
    if isranking:
        return(np.argsort(counts)[::-1])
    else:
        return(np.argsort(counts))