from config import RATIO_TEST
from sklearn.model_selection import StratifiedShuffleSplit
import pandas as pd
import numpy as np

np.random.seed(1234)

version = 'v2'

#%%
for cohort in ['national', 'priority']:
    for gender in ['females', 'males']:

        dataset = f'{gender}_{cohort}'
        
        #%
        features = pd.read_csv(f'./Data/CSV/datasets/{version}/{dataset}/features.csv', index_col=0)
        target = pd.read_csv(f'./Data/CSV/datasets/{version}/{dataset}/targets.csv', index_col=0)
        print(features.shape)
        #%
        ss = StratifiedShuffleSplit(1, test_size=RATIO_TEST)
        idx_train, idx_test = next(ss.split(features, target))
        
        np.savetxt(f'./Data/CSV/datasets/{version}/{dataset}/idx_train.txt', idx_train, fmt='%d')
        np.savetxt(f'./Data/CSV/datasets/{version}/{dataset}/idx_test.txt', idx_test, fmt='%d')
