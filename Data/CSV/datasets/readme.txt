This folder will contain 4 subfolders, one for each cohort investigated in the study:
- females_national
- females_priority
- males_national
- males_priority

Each subfolder will contain the following files (see template folder)
- features.csv: the variables used to make the predictions
- target.csv: the expected outcome
- idx_train: datapoints to be used to train the model
- idx_test: datapoints to be used to test the model

The 4 subfolders and respective files are created by executing the following scripts: 
1. 002_split_features_target.py
2. 003_split_train_test.py
